//
//  WeatherModelTests.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "WeatherModel.h"

SpecBegin(WeatherModel)
describe(@"Weather Model Should", ^{
    it(@"Not create object for empty data", ^{
        expect([[WeatherModel alloc] initWithDictionary:nil]).to.beNil();
    });

    it(@"Not create object for bad data", ^{
        id malformedObject = [NSNull null];
        expect([[WeatherModel alloc] initWithDictionary:malformedObject]).to.beNil();
    });

    describe(@"Not mapped null object instance for", ^{
        __block WeatherModel *_model;
        beforeAll(^{
            _model = [[WeatherModel alloc] initWithDictionary:@{@"atmosphere":[NSNull null],
                                                                @"item":@{@"condition":[NSNull null],
                                                                          @"description":[NSNull null]
                                                                          },
                                                                @"units":[NSNull null]
                                                                }];
        });

        it(@"Atmosphere", ^{
            expect(_model.atmospherePressure).to.beNil();
        });

        it(@"Temperature", ^{
            expect(_model.temperature).to.beNil();
        });

        it(@"Weather condition image", ^{
            expect(_model.wetherConditionsUrl).to.beNil();
        });

        it(@"Atmosphere unit", ^{
            expect(_model.atmospherePressureUnit).to.beNil();
        });

        it(@"Temperature unit", ^{
            expect(_model.temperatureUnit).to.beNil();
        });
    });


    describe(@"Be import valid data for", ^{
        __block WeatherModel *_model;
        beforeAll(^{
            _model = [[WeatherModel alloc] initWithDictionary:@{@"atmosphere":@{@"humidity":@(61),
                                                                                @"pressure":@"982,05"},
                                                                @"item":@{@"condition":@{@"temp":@(25)},
                                                                          @"description":@"\n<img src=\"http://l.yimg.com/a/i/us/we/52/30.gif\"/><br />\n<b>Current Conditions:</b><br />\nPartly Cloudy, 25 C<BR />\n<BR /><b>Forecast:</b><BR />\nTue - Partly Cloudy. High: 31 Low: 17<br />\nWed - Mostly Sunny. High: 24 Low: 13<br />\nThu - Partly Cloudy. High: 25 Low: 13<br />\nFri - Sunny. High: 26 Low: 14<br />\nSat - Sunny. High: 29 Low: 17<br />\n<br />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Toguchinskiy_Raion__RU/*http://weather.yahoo.com/forecast/RSXX0276_c.html\">Full Forecast at Yahoo! Weather</a><BR/><BR/>\n(provided by <a href=\"http://www.weather.com\" >The Weather Channel</a>)<br/>\n"
                                                                          },
                                                                @"units":@{@"distance":@"km",
                                                                           @"pressure":@"mb",
                                                                           @"speed":@"km/h",
                                                                           @"temperature":@"C"}
                                                                }];
        });

        it(@"Atmosphere pressure" , ^{
            expect(_model.atmospherePressure).to.equal(@"982,05");
        });

        it(@"Temperature value", ^{
            expect(_model.temperature).to.equal(@"25");
        });

        it(@"Weather image status URL", ^{
            expect(_model.wetherConditionsUrl).to.equal(@"http://l.yimg.com/a/i/us/we/52/30.gif");
        });

        it(@"Atmosphere pressure unit", ^{
            expect(_model.atmospherePressureUnit).to.equal(@"mb");
        });

        it(@"Temperature unit", ^{
            expect(_model.temperatureUnit).to.equal(@"C");
        });
    });
});

SpecEnd
