//
//  main.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
