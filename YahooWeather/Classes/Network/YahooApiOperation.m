//
//  YahooApiClient.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "YahooApiOperation.h"
#import "UIAlertView+NSError.h"

#define QUERY_PREFIX @"https://query.yahooapis.com/v1/public/yql?q="
#define QUERY_SUFFIX @"&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="

#define kWeatherURL @"select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='%@,%@') and u='c'"

#define kTimeOutInterval 60.f

@interface YahooApiOperation()<NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
    NSMutableData *_data;
    NSURLConnection *_connection;
    
}

@property (nonatomic, strong) ApiCompletion completionBlock;

@end

@implementation YahooApiOperation

+ (NSOperationQueue *)downloadQueue {
    static NSOperationQueue *operation = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        operation = [[NSOperationQueue alloc] init];
    });

    return operation;
}

- (instancetype)initWeatherRequestWithCountry:(NSString *)country city:(NSString *)city completion:(ApiCompletion)completion {
    if (self = [super init]) {
        _completionBlock = [completion copy];
         NSURL *url = [self urlForQuery:[NSString stringWithFormat:kWeatherURL,country,city]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:kTimeOutInterval];

        NSURLConnection *connetcion = [NSURLConnection connectionWithRequest:request delegate:self];
        if ([connetcion respondsToSelector:@selector(setDelegateQueue:)]) {
            [connetcion setDelegateQueue:[[self class] downloadQueue]];
        }

        _connection = connetcion;
        _data = [NSMutableData new];
    }
    return self;
}

#pragma mark -

- (NSURL *)urlForQuery:(NSString *)query {
     NSString *url = [NSString stringWithFormat:@"%@%@%@", QUERY_PREFIX, [query stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding], QUERY_SUFFIX];
    return [NSURL URLWithString:url];
}

- (void)cancel {
    _data = nil;
    _completionBlock = nil;
    [_connection cancel];
}

- (void)start {
    [_connection start];
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

    __block typeof(self) self_weak = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self_weak.completionBlock) {
            self_weak.completionBlock(error,nil);
            self_weak.completionBlock = nil;
        }
    });
}
#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_data appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *error;
    NSDictionary *responce = [NSJSONSerialization JSONObjectWithData:_data options:0 error:&error];
    __block typeof(self) self_weak = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self_weak.completionBlock) {
            self_weak.completionBlock(error,responce);
            self_weak.completionBlock = nil;
        }
    });
}

@end
