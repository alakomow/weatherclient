//
//  YahooApiClient.h
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ApiCompletion)(NSError *error, NSDictionary *responce);

@interface YahooApiOperation : NSObject
- (instancetype)initWeatherRequestWithCountry:(NSString *)country city:(NSString *)city completion:(ApiCompletion)completion;

- (void)start;
- (void)cancel;
@end
