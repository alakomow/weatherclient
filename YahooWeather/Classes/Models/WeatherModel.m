//
//  WeatherModel.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "WeatherModel.h"

#define kKeyAtmosphere @"atmosphere"
#define kKeyPressure @"pressure"
#define kKeyItem @"item"
#define kKeyCondition @"condition"
#define kKeyTemp @"temp"
#define kKeyUnits @"units"
#define kKeyTemperature @"temperature"
#define kKeyDescription @"description"
#define kKeyImg @"img"
#define kKeySrc @"src"

@interface WeatherModel()<NSXMLParserDelegate>

@end

@implementation WeatherModel
- (instancetype)initWithDictionary:(NSDictionary *)data {
    if (self = [super init]) {
        if (![data isKindOfClass:[NSDictionary class]]) {
            return nil;
        }
        id value = [data objectForKey:kKeyAtmosphere];
        if ([value isKindOfClass:[NSDictionary class]]) {
            id pressure = [value objectForKey:kKeyPressure];
            if (![pressure isEqual:[NSNull null]]) {
                _atmospherePressure = [NSString stringWithFormat:@"%@",pressure];
            }
        }

        value = [data objectForKey:kKeyItem];
        if ([value isKindOfClass:[NSDictionary class]]) {
            id condotion = [value objectForKey:kKeyCondition];
            if ([condotion isKindOfClass:[NSDictionary class]]) {
                id temp = [condotion objectForKey:kKeyTemp];
                if (![temp isEqual:[NSNull null]]) {

                    _temperature = [NSString stringWithFormat:@"%@",temp];
                }
            }

            value = [value objectForKey:kKeyDescription];
            if ([value isKindOfClass:[NSString class]]) {
                NSXMLParser *xmpParser = [[NSXMLParser alloc] initWithData:[value dataUsingEncoding:NSUTF8StringEncoding]];
                xmpParser.delegate = self;
                [xmpParser parse];
            }
        }

        value = [data objectForKey:kKeyUnits];
        if ([value isKindOfClass:[NSDictionary class]]) {
            id unit = [value objectForKey:kKeyPressure];
            if (![unit isEqual:[NSNull null]]) {
                _atmospherePressureUnit = [NSString stringWithFormat:@"%@",unit];
            }

            unit = [value objectForKey:kKeyTemperature];
            if (![unit isEqual:[NSNull null]]) {
                _temperatureUnit = [NSString stringWithFormat:@"%@",unit];
            }
        }



    }
    return self;
}

#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:kKeyImg]) {
        id imgSrc = [attributeDict objectForKey:kKeySrc];
        if ([imgSrc isKindOfClass:[NSString class]]) {
            _wetherConditionsUrl = imgSrc;
            [parser abortParsing];
        }
    }
}
@end
