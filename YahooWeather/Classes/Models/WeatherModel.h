//
//  WeatherModel.h
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherModel : NSObject
@property (nonatomic, strong, readonly) NSString *temperature;
@property (nonatomic, strong, readonly) NSString *temperatureUnit;
@property (nonatomic, strong, readonly) NSString *atmospherePressure;
@property (nonatomic, strong, readonly) NSString *atmospherePressureUnit;
@property (nonatomic, strong, readonly) NSString *wetherConditionsUrl;
- (instancetype)initWithDictionary:(NSDictionary *)data;
@end
