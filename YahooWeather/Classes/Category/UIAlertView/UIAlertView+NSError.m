//
//  UIAlertView+NSError.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "UIAlertView+NSError.h"

@implementation UIAlertView (NSError)
+ (void)showWithError:(NSError *)error {
    UIAlertView *alert = [[self alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
    [alert show];
}
@end
