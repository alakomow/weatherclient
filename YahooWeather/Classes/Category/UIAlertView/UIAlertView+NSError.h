//
//  UIAlertView+NSError.h
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (NSError)
+ (void)showWithError:(NSError *)error;
@end
