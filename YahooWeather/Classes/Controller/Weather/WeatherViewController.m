//
//  WeatherViewController.m
//  YahooWeather
//
//  Created by Artem on 14/07/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "WeatherViewController.h"
#import "UIAlertView+NSError.h"
#import "YahooApiOperation.h"
#import "WeatherModel.h"

@interface WeatherViewController () <CLLocationManagerDelegate,MKReverseGeocoderDelegate>
@property (nonatomic, strong, readonly) CLLocationManager *locationManager;
@property (nonatomic, strong) YahooApiOperation *apiOperation;
@property (nonatomic, strong) WeatherModel *weather;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *atmospherePressureLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *locationLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *weatherConditionImageView;
@end

@implementation WeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    self.weather = [WeatherModel new];

    _locationManager = [CLLocationManager new];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.delegate = self;
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)reloadData:(id)sender {
    self.weather = [WeatherModel new];
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Setters
- (void)setWeather:(WeatherModel *)weather {
    _weather = weather;
    self.temperatureLabel.text = [self.weather.temperature stringByAppendingFormat:@" %@",self.weather.temperatureUnit];
    self.atmospherePressureLabel.text = [self.weather.atmospherePressure stringByAppendingFormat:@" %@",self.weather.atmospherePressureUnit];

    __block typeof(self) self_block_ = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self_block_.weather.wetherConditionsUrl]];
        UIImage *image = [UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            self_block_.weatherConditionImageView.image = image;
        });
    });
}

#pragma mark -
- (void)reloadGeoForLocation:(CLLocation *)location {

    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_4_3) {
        CLGeocoder *geocoder = [CLGeocoder new];
        __block typeof(self) self_block_ = self;
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error)
             {
                 [self_block_ geocoderDidError:error];
             } else if(placemarks.count > 0)
             {
                 CLPlacemark * placemark = placemarks.firstObject;
                 [self_block_ geocoderDidFoundPlace:placemark];
             }
         }];
    } else {
        MKReverseGeocoder *geocoder = [MKReverseGeocoder new];
        geocoder.delegate = self;
        [geocoder start];
    }
}

- (void)reloadWeatherForCountry:(NSString *)country city:(NSString *)city {
    self.locationLabel.text = [country stringByAppendingFormat:@", %@",city];
    __block typeof(self) self_block_ = self;
    self.apiOperation = [[YahooApiOperation alloc] initWeatherRequestWithCountry:country city:city completion:^(NSError *error, NSDictionary *responce) {

        if (error) {
            [UIAlertView showWithError:error];
            return;
        }

        id data = [responce objectForKey:@"query"];
        if ([data isKindOfClass:[NSDictionary class]]) {
            if ([[data objectForKey:@"count"] boolValue]) {
                id results = [data objectForKey:@"results"];
                if ([results isKindOfClass:[NSDictionary class]]) {
                    self_block_.weather = [[WeatherModel alloc] initWithDictionary:[results objectForKey:@"channel"]];
                }
            }
        }
    }];
}

#pragma mark - CLLocationManagerDelegate responce wrapper
- (void)cllocationManagerDidUpdateLocation:(CLLocation *)location {
    [self reloadGeoForLocation:location];
}

#pragma mark - geocoder responce wrappers

- (void)geocoderDidFoundPlace:(CLPlacemark *)placeMark {
    NSString *country = placeMark.country;
    NSString *city = placeMark.locality;
    if (!city) {
        city = placeMark.subAdministrativeArea;
    }
    if (!city) {
        city = placeMark.administrativeArea;
    }
    [self reloadWeatherForCountry:country city:city];
}

- (void)geocoderDidError:(NSError *)error {
    [UIAlertView showWithError:error];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [manager stopUpdatingLocation];
    [UIAlertView showWithError:error];

}

- (void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error {
    [manager stopUpdatingLocation];
    [UIAlertView showWithError:error];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [manager stopUpdatingLocation];
    [self cllocationManagerDidUpdateLocation:locations.firstObject];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [manager stopUpdatingLocation];
    [self cllocationManagerDidUpdateLocation:newLocation];
}

#pragma mark - MKReverseGeocoderDelegate
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark {
    [self geocoderDidFoundPlace:placemark];
}
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error {
    [self geocoderDidError:error];
}

@end
